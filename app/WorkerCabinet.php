<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerCabinet extends Model
{
    protected $table = "worker_cabinets";
    public $timestamps = false;

}
