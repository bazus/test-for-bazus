<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use PHPHtmlParser\Dom;

final class Employee extends Model
{
    protected  $table = "worker";

    public $timestamps = false;

    public function cabinet(){
        return $this->hasOneThrough(
            '\App\Cabinet',
            '\App\WorkerCabinet',
            'workerId',
            'id',
            'id',
            'cabinetId'
        );
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
//        $this->_fill();
//        $this->_create();
    }

    /**
     * Show all files from docs/userId
     */
    public function showFiles(){
        $files = Storage::disk('docs')->files($this->id);

        var_dump($files);

        $result =  array_filter(
            $files,
            function ($item) {
                $regular = '/^(\w|(0-9))+.txt$/';
                return preg_match($regular, basename($item) );

            }
        );
        sort( $result );

        foreach ($result as $file) {
            echo basename($file).PHP_EOL;
        }
    }

    /**
     * Gets image url from user page in vk.com and sets it to filed photo
     *
     * @throws \PHPHtmlParser\Exceptions\ChildNotFoundException
     * @throws \PHPHtmlParser\Exceptions\CircularException
     * @throws \PHPHtmlParser\Exceptions\LogicalException
     * @throws \PHPHtmlParser\Exceptions\NotLoadedException
     * @throws \PHPHtmlParser\Exceptions\StrictException
     */
    public function getPhoto( ){
        $dom = new Dom;
        $dom->loadFromFile('https://vk.com/id' . $this->vkid);
        $contents = $dom->find('.page_avatar_img');
        $this->photo = $contents[0]->getAttribute('src');
    }

    /**
     * Gets worker from the biggest/smallest cabinet
     *
     * If $small == false gets worker from the biggest cabinet
     *
     * @param bool $small
     * @return array
     */
    public static function getFromTheMost($small = true){

        $query = $small ? Cabinet::min('capacity') : Cabinet::max('capacity');

        $result = [];

        foreach (Cabinet::where('capacity', $query)->get() as $cabinet ) {
            array_push($result, $cabinet->workers);
        }
        return $result;

    }

    /**
     * Gets the richest worker on floor/cabinet
     *
     * If param $isCabinet == true function finds the richest in cabinet
     *
     * @param $number
     * @param bool $isCabinet
     * @return mixed
     */
    public static function getTheRichest($number, $isCabinet = false){

        if( $isCabinet ){
            $workers = Cabinet::find($number)->workers;
            usort($workers, function($worker1, $worker2) {
                if($worker1->salary == $worker2->salary) return 0;
                return ($worker1->salary > $worker2->salary) ? -1 : 1;});

            return $workers[0];
        }else {
            return self::getWorkersOnFloorQuery( $number )->orderBy('salary', 'desc')->first() ;
        }
    }

    /**
     * Gets all workers on floor
     *
     * @param $floor
     * @param string $query
     * @return mixed
     */
    public static function getWorkersOnFloor($floor, $query = "*"){
        return self::getWorkersOnFloorQuery($floor)->get($query);
    }

    /**
     * Creates database
     */
    private function _create(){

        // php artisan migrate

    }

    /**
     * Fills database with random data
     */
    private function _fill(){
        $seeder = new \Fill();
        $seeder->run(15);

        // or php artisan db:seed
    }


    private static function getWorkersOnFloorQuery($floor){
        return Employee::whereIn('id',
            WorkerCabinet::whereIn('cabinetId',
                Cabinet::where('floor', $floor)->get('id') )->get('workerId')
        );
    }

}
