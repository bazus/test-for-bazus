<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabinet extends Model
{
    protected $table = "cabinet";
    public $timestamps = false;

    public function workers(){
        return $this->belongsToMany(
            '\App\Employee',
            'worker_cabinets',
            'cabinetId',
            'workerId'
        );
    }
}
