<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'name'          => $faker->name,
        'tel'           => $faker->e164PhoneNumber,
        'address'       => $faker->address,
        'salary'        => rand(1000,10000),
        'vkid'          => rand(100000,1000000)
    ];
});
