<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cabinet;
use Faker\Generator as Faker;

$autoIncrement = autoIncrement();

$factory->define(Cabinet::class, function (Faker $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'num'       => $autoIncrement->current(),
        'floor'     => $faker->numberBetween(1,10),
        'capacity'  => rand(1,4)
    ];
});



function autoIncrement()
{
    for ($i = 0; $i < 1000; $i++) {
        yield $i;
    }
}
