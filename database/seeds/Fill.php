<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;


class Fill extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param int $count
     * @return void
     */
    public function run($count = 3)
    {
        factory(App\Cabinet::class, $count)
            ->create()
            ->each(function ($cabinet) {
                factory(App\Employee::class, $cabinet->capacity)->create()

                    ->each(function ($employee) use ($cabinet) {

                        factory(App\WorkerCabinet::class, 1)->create([
                            'workerId'  => $employee->id,
                            'cabinetId' => $cabinet->id

                        ]);
                        Storage::disk('docs')->makeDirectory($employee->id);
                    });

            });


    }
}
